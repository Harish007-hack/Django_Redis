# Django_Redis

## Virtual environment 
`python3 -m venv ./env`

`source ./env/bin/activate`

## Installing 

`pip install -r requirements.txt`

## Prerequisites
1. Redis is running on localhost (or Docker with 6379:6379 exposed) 

## Running Django server

`python manage.py runserver`

## Testing the API 

`curl --location --request DELETE 'http://localhost:8000/api/user/ma5KS5q8tV1CbIERR1nV/cache'`

## Testing queue content

```shell script
redis-cli

127.0.0.1:6379> LLEN com_sum
(integer) 1

127.0.0.1:6379> LINDEX com_sum 0
"ma5KS5q8tV1CbIERR1nV"


```