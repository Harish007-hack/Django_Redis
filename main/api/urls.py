from django.urls import path
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('all-user-info', all_user_info, name='User-info'),
    path('user/<str:user_id>/cache', user_delete, name='delete'),
]
