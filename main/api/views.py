from django_redis import get_redis_connection
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .serializers import *

redis = get_redis_connection("default")


# Create your views here.
@api_view(['GET'])
def home(request):
    Data = {
        "Home-Details":"api/",
        "All-User-Details": "api/all-user-info",
        "User-Delete":"api/delete"
    }
    return Response(Data)



@api_view(['GET'])
def all_user_info(request):
    users = User.objects.all()
    serializers  = UserSerializers(users,many=True)
    return Response(serializers.data)


# noinspection PyInterpreter
@api_view(['DELETE'])
def user_delete(request, user_id, *args, **kwargs):
    # https://redis.io/commands/lpush
    redis.lpush("com_sum", user_id)
    response = {
        'msg': " cache invalidation done"
    }
    return Response(response, status=200)
